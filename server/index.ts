import { createServer } from 'http'
import { parse } from 'url'
import { Game } from "../lib/Game"
import { Server as BoardGameServer } from "boardgame.io/server"
import next from 'next'

const port = parseInt(process.env.PORT || '4000', 10)
const gamePort = parseInt(process.env.GAME_PORT || '8000', 10)
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const bgioServer = BoardGameServer({ games: [Game] });
bgioServer.run(gamePort).then(() => {
  console.log(
    `> Boardgameio listening at http://localhost:${gamePort} as ${
      dev ? 'development' : process.env.NODE_ENV
    }`
  )
})

app.prepare().then(() => {
  createServer((req, res) => {
    const parsedUrl = parse(req.url!, true)
    const { pathname, query } = parsedUrl

    if (pathname === '/a') {
      app.render(req, res, '/a', query)
    } else if (pathname === '/b') {
      app.render(req, res, '/b', query)
    } else {
      handle(req, res, parsedUrl)
    }
  }).listen(port)

  // tslint:disable-next-line:no-console
  console.log(
    `> Server listening at http://localhost:${port} as ${
      dev ? 'development' : process.env.NODE_ENV
    }`
  )
})
