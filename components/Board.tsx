import equal from 'deep-equal';
import { getTiles, getVertices } from 'lib/util';
import { stringify } from 'querystring';
import React, { useState } from 'react';
import { HexGrid, Layout, Pattern} from 'react-hexgrid';
import { PlayerCards} from './Cards'
import { countPoints, GameState, getBuildableRoads, PlayerHand, PlayerOwned } from '../lib/Game';
import { Cell, DevCard, EdgeIndex, Hex, Resource, VertexIndex, TileIndex, Port } from '../lib/types';
import { pointsToDots, Tile } from "./Tile";
import type { Ctx , PlayerID } from 'boardgame.io';
import {CatanFiles as Filenames} from "../lib/strings"

function hexToPixel(hex: TileIndex) {
    const s = layout.spacing;
    const M = layout.orientation;
    let x = (M.f0 * hex.q + M.f1 * hex.r) * layout.size.x;
    let y = (M.f2 * hex.q + M.f3 * hex.r) * layout.size.y;
    // Apply spacing
    x = x * s;
    y = y * s;
    return {'x': x + layout.origin.x, 'y': y + layout.origin.y};
  }
function hexCornerToPixel(hex: VertexIndex) {
    const s = layout.spacing;
    const M = layout.orientation;
    let x = (M.f0 * hex.q + M.f1 * hex.r) * layout.size.x;
	let y = (M.f2 * hex.q + M.f3 * hex.r) * layout.size.y;
	if(hex.orientation == "SOUTHEAST") {
		x = x + 0.5* (layout.size.x * M.f0)
		y = y + Math.sqrt(2)/4 * (layout.size.y * M.f3)
	}
	if(hex.orientation == "NORTHEAST") {
		x = x + 0.5* (layout.size.x * M.f0)
		y = y - Math.sqrt(2)/4 * (layout.size.y * M.f3)
	}
    // Apply spacing
    x = x * s;
    y = y * s;
    return {'cx': x + layout.origin.x, 'cy': y + layout.origin.y};
}
function hexEdgeToPixels(edge: EdgeIndex) {
	let [p1, p2] : VertexIndex[] = getVertices(edge);
	const px1 = hexCornerToPixel(p1)
	const px2 = hexCornerToPixel(p2)
    return {x1: px1.cx, y1: px1.cy, x2: px2.cx, y2: px2.cy}
}
const playerColors = ["red", "blue", "green", "yellow", "black", "purple"]

function Robber({position}) {
	const width = 6
	return (<image xlinkHref={"robber.png"} {...hexToPixel(position)} width={width} height={width} transform="translate(0.5,-4.0)" />)
}
function Point({vertexPosition, click}) {
	return (<circle r={1} {...hexCornerToPixel(vertexPosition)} onClick={() => click(vertexPosition)}/>)
}
function Ship({port} : {port : Port}) {
	const {center, edge } = port;
	const radius = 0.95
	const shipCenter = {q: radius * center.q, r: radius * center.r, s: radius * center.s}
	const width = 7;
	const docks = hexEdgeToPixels(edge);
	const shipPos = hexToPixel(shipCenter)
	const color = "bisque";
	const ratio = port.resource == Resource.Desert ? "3:1" : "2:1"
	return (
		<>
			<image xlinkHref={"sailboat.png"} {...hexToPixel(shipCenter)} width={width} height={width} transform={`translate(-${width / 2},-${width / 2})`} />
		<line
			x1={shipPos.x}
			y1={shipPos.y}
			x2={docks.x1}
			y2={docks.y1}
			stroke={color}
			strokeWidth={0.4}
			strokeDashoffset={"1"}
			strokeDasharray={"1"}
		/>
		<line
			x1={shipPos.x}
			y1={shipPos.y}
			x2={docks.x2}
			y2={docks.y2}
			stroke={color}
			strokeWidth={0.4}
			strokeDashoffset={"1"}
			strokeDasharray={"1"}
		/>
			<circle r={1.0} fill={color} cx={docks.x1} cy={docks.y1} />
			<circle r={1.0} fill={color} cx={docks.x2} cy={docks.y2} />
			{port.resource != Resource.Desert &&
				<image className={"port-resource"} xlinkHref={Filenames[port.resource]} {...hexToPixel(shipCenter)} width={width * 0.8} height={width * 0.8} transform={`translate(-${width / 2},-${width / 2})`} />
			}
			<text className={"trading-value"} x={shipPos.x} y={shipPos.y} transform={`translate(-${width / 3},+${width})`}>{ratio}</text>
	</>
	)
}
function Buildable({position, click, points}) {
	// const pos_props = (position instanceof TileIndex) ? hexCornerToPixel(position) : hexEdgeToPixel(position);
	const pos_props = hexCornerToPixel(position);
	return (
	<circle
		r={1 + (points ? points/12.0: 0)}
		{...pos_props}
		onClick={() => click(position)}
		>
			{points && <title>{points}</title>}
		</circle>
	)
}
function BuildableRoad({position, click, playerID}: {position: EdgeIndex, click: Function, playerID: PlayerID}) {
	return (
		<line
			key={stringify(position)}
			{...hexEdgeToPixels(position)}
			onClick={() => click(position)}
			stroke={"grey"}
			strokeWidth={1.3}
			strokeDashoffset={"1"}
			strokeDasharray={"1"}
			strokeLinecap={"round"}
			pathLength={1}
			style={{animation:"dash 2s linear alternate infinite"}}
			/>
	)
}

function Farm({vertexPosition, click, playerID}) {
	const width = 3.5;
	const pos = hexCornerToPixel(vertexPosition);
	return (<>
	<circle
		r={width/2 + 0.2}
		cx={pos.cx}
		cy={pos.cy}
		stroke={playerColors[playerID]}
		strokeWidth={0.5}
		fill={'white'}
		>
	</circle>
	<image
		// className={`farm-img-${player_id}`}
		className={`farm-player`}
		xlinkHref={"farm.png"}
		x={pos.cx} y={pos.cy}
		width={width} height={width}
		transform={`translate(-${width/2},-${width/2})`}
		onClick={() => click(vertexPosition)}
		/>
	</>
		)
}
function City(props: {vertexPosition: VertexIndex, playerID: PlayerID}) {
	const {vertexPosition, playerID} = props;
	const width = 4;
	const pos = hexCornerToPixel(vertexPosition);
	return (<>
		<circle
			r={width / 2 + 0.2}
			cx={pos.cx} cy={pos.cy}
			stroke={playerColors[playerID]}
			strokeWidth={0.5}
			fill={'none'}
		></circle>
		<image
			xlinkHref={"city.png"}
			x={pos.cx} y={pos.cy}
			width={width} height={width}
			transform={`translate(-${width / 2},-${width / 2})`}
		/>
	</>)
}
function Road(road: PlayerOwned<EdgeIndex>) {
	return (
		<line
			key={stringify(road.idx)}
			{...hexEdgeToPixels(road.idx)}
			stroke={playerColors[road.player]}
			strokeLinecap={"round"}
			strokeWidth={1.1}
		/>
	)
}
function ResourceSelector({choose}: {choose: Function}) {
	const resources = [Resource.Sheep, Resource.Wheat, Resource.Ore, Resource.Brick, Resource.Tree]
	return (<ul>
		{resources.map(res => {
			return (<button onClick={() => choose(res)}>{res}</button>)
		})}
	</ul>
	)
}
const findCellByPosition = (cells: Cell[], pos: Hex) : Cell | undefined => {
	return cells.find(c => equal(c.position, pos))
}
enum MODE {
	PLAYING,
	BUILDING_ROAD,
	BUILDING_FARM,
	BUILDING_CITY,
	PLACING_ROBBER,
	CHOOSING_RESOURCE,
	CHOOSING_RESOURCES,
	CHOOSING_ROADS,
}
const orientation = {
	f0: Math.sqrt(3.0),
	f1: Math.sqrt(3.0) / 2.0,
	f2: 0.0,
	f3: 3.0 / 2.0,
	b0: Math.sqrt(3.0) / 3.0,
	b1: -1.0 / 3.0,
	b2: 0.0,
	b3: 2.0 / 3.0,
	startAngle: 0.5
};
const layout_size = { x: 10, y: 10 };
const spacing = 1.1;
const layout = {orientation, spacing, origin: {x: 0, y: 0}, size: layout_size}

export function Board({G, moves, ctx, playerID, events}: {G: GameState, moves: object, ctx: Ctx, playerID: PlayerID, events: object}) {
	//soon we'll turn these into a useReducer
	const [mode, setMode] = useState<MODE>(MODE.PLAYING);
	const [firstResource, setFirstResource] = useState<Resource>();
	const [firstRoad, setFirstRoad] = useState<EdgeIndex>();
	const cells : Cell[] = G.cells;
	const robber : Hex = G.robber;
	const hand: PlayerHand = G.player_hands[playerID];
	const buildable_vertices : VertexIndex[] = G.buildable_vertices;
	const cities : PlayerOwned<VertexIndex>[] = G.cities;
	const farms : PlayerOwned<VertexIndex>[] = G.settlements;
	// const roads = Array.from(G.roads).map(r => JSON.parse(r))
	// const roads = G.roads.entries()
	const player_points = Array(ctx.numPlayers).fill(0).map( (_,idx) => countPoints(G, idx.toString(), false));
	const winner = ctx.gameover?.winner;
	const private_points = countPoints(G, playerID, true)

	const belongsTo = <T extends {}>(player_owned: PlayerOwned<T>) => player_owned.player == ctx.currentPlayer

	const chooseResource = (resource: Resource) => {
		if (firstResource) {
			setMode(MODE.PLAYING);
			moves.useDevCard(DevCard.YearOfPlenty, { res_1: firstResource, res_2: resource })
			setFirstResource(undefined)
		} else {
			setFirstResource(resource)
		}
	}
	const chooseRoad = (position: EdgeIndex) => {
		if (firstRoad) {
			setMode(MODE.PLAYING);
			moves.useDevCard(DevCard.RoadBuilding, { road_1: firstRoad, road_2: position })
			setFirstRoad(undefined)
		} else {
			setFirstRoad(position)
		}
	}
	const onDevCardClick = (c: DevCard) => {
		if (c == DevCard.Knight) {
			setMode(MODE.PLACING_ROBBER)
		}
		if (c == DevCard.Monopoly) {
			setMode(MODE.CHOOSING_RESOURCE)
		}
		if (c == DevCard.RoadBuilding) {
			setMode(MODE.CHOOSING_ROADS)
		}
		if (c == DevCard.YearOfPlenty) {
			setMode(MODE.CHOOSING_RESOURCES)
		}

	}
	console.log("React drawing board")
	return (
		<>
		{winner != undefined && (<p>Player {winner} has won!</p>)}
		<div className={"board"}>
			<HexGrid width={1200} height={700}>
				<Layout size={layout_size} flat={false} spacing={spacing} >
					{ cells.map((hex, i) => (<Tile key={i} {...hex}/>)) }
					{ G.ports.map((port, i) => (<Ship key={i} port={port}/>)) }
					{ mode == MODE.BUILDING_FARM &&
							[...buildable_vertices].map(v => Buildable({
								position: v,
								click: (pos) => { setMode(MODE.PLAYING); moves.buildSettlement(pos) },
								points: getTiles(v).map(t => pointsToDots[findCellByPosition(cells, t)?.points ?? 0]).reduce((a, b) => b ? a + b : a, 0)
						})
					)}
					{ mode == MODE.PLACING_ROBBER &&
							cells.map(c => Buildable({
							position: c.position,
							click: (position) => { setMode(MODE.PLAYING); moves.useDevCard(DevCard.Knight, {position})},
							points: undefined,
							}))
					}
					{mode == MODE.BUILDING_ROAD &&
						getBuildableRoads(G, ctx.currentPlayer).map(edge => BuildableRoad({
								position: edge,
								click: (position) => { setMode(MODE.PLAYING); moves.buildRoad(position)},
								playerID: ctx.currentPlayer
							}))
					}
					{firstRoad && <Road player={playerID} idx={firstRoad} />}
					{mode == MODE.CHOOSING_ROADS &&
						getBuildableRoads(G, ctx.currentPlayer, firstRoad).map(edge => BuildableRoad({
								position: edge,
								click: (position) => { chooseRoad(position)},
								playerID: ctx.currentPlayer
							}))
					}
					{[...farms ].map(v => Farm({vertexPosition: v.idx, playerID: v.player, click: (pos) => {if(mode == MODE.BUILDING_CITY) {setMode(MODE.PLAYING); moves.buildCity(pos)}}}))}
					{Object.entries(G.roads).map(([s, id]) => Road({ player: id, idx: JSON.parse(s) }))}
					{[...cities].map(v => City({vertexPosition: v.idx, playerID: v.player}))}
					<Robber position={robber} />
					{[Resource.Brick, Resource.Desert, Resource.Tree, Resource.Sheep, Resource.Wheat, Resource.Ore].map(r => {
						return <Pattern id={r} link={Filenames[r]} size={layout_size} />
					})}
				</Layout>
			</HexGrid>
		</div>
		{G.rolls.length > 0 && (
			<span className={"dice"}>
				(Last roll was {G.rolls[G.rolls.length -1 ]})
			</span>
		)}
		<PlayerCards resources={hand.resources} devcards={hand.devcards} onClick={onDevCardClick} />
		<div className={"gameInfo"}>
			{G.largest_army && (<p>Player {G.largest_army} has largest army.</p>)}
			{G.longest_road && (<p>Player {G.longest_road} has longest road.</p>)}
			<ul className={"pointsList"}>
				{player_points.map((p, idx) => (
				<li key={idx} className={"points"} style={{listStyle: 'none', color:playerColors[idx]}}>
					{idx == parseInt(playerID) ? `You have ${p} ${private_points != p ? "(" + private_points + ")" : ""} points` : `Player ${idx} has ${p} points`}
				</li>))}
			</ul>
		</div>
		{ctx.currentPlayer == playerID && <div className={"controls"}>
			{mode == MODE.CHOOSING_RESOURCE && <ResourceSelector choose={(resource: Resource) =>{setMode(MODE.PLAYING); moves.useDevCard(DevCard.Monopoly, {resource})}}/>}
			{mode == MODE.CHOOSING_RESOURCES && (
				<>
				<ResourceSelector choose={chooseResource} />
				<ResourceSelector choose={chooseResource}/>
				</>
			)}
			{mode == MODE.PLAYING && <button onClick={() => setMode(MODE.BUILDING_ROAD)}>Build Road</button>}
			{mode == MODE.PLAYING && <button onClick={() => setMode(MODE.BUILDING_FARM)}>Build Farm</button>}
			{mode == MODE.PLAYING && <button onClick={() => setMode(MODE.BUILDING_CITY)}>Build City</button>}
			{mode == MODE.PLAYING && <button onClick={() => moves.buyDevCard()}>Dev Card</button>}
			{mode == MODE.PLAYING && <button onClick={() => moves.rollDice()}>Roll Dice</button>}
			{mode != MODE.PLAYING && <button onClick={() => setMode(MODE.PLAYING)}>Cancel Move</button>}
			{mode == MODE.PLAYING && <button onClick={() => events.endTurn()}>End Turn</button>}
		</div>
		}
		</>
	       )
}
