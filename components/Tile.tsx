import { Resource } from 'lib/types';
import React from 'react';
import { Hexagon, Text, Hex } from 'react-hexgrid';

export const pointsToDots: { [index: number]: number } = {
    0: 0,
    2: 1,
    3: 2,
    4: 3,
    5: 4,
    6: 5,
    7: 6,
    8: 5,
    9: 4,
    10: 3,
    11: 2,
    12: 1,
}
export function Tile({position, points, resource, key}: {position: Hex, points: number, resource: Resource, key: string}) {
    return (
    <Hexagon fill={resource} key={key} {...position}>
        {points && <Text className={"tilePoints"}>{points}</Text>}
        <g transform="translate(0,3)">
        <Text className={"tileDots"}>{'•'.repeat(pointsToDots[points])}</Text>
        </g>
    </Hexagon>
    )
}