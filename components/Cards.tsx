import { CardStack } from "lib/CardStack";
import {CatanStrings as Strings} from "lib/strings"
import { DevCard, Resource } from "lib/types";

//This probably should be some kind of event, or in a reducer or something
export function PlayerCards(props: {devcards: CardStack<DevCard>, resources: CardStack<Resource>, onClick: Function}) {
	const {devcards, resources, onClick} = props
	const resource_cards = Object.entries(resources).map(([resource, count], id) => {
		if(count < 1) {
			return;
		}
		return (
			<span className="resource-card" key={`res-${id++}`}>
				<div>
				{Strings[resource]}
				{(<div className="card-count">({count})</div>)}
				</div>
			</span>
		);
	}
	);
	const dev_cards = Object.entries(devcards).map(([c, count], id) => {
		if(count < 1) {
			return;
		}
		let classes = ["resource-card"]
		if (c == DevCard.VictoryPoint) {
			classes.push("disabled")
		}
		return (
			<span className={classes.join(' ')} key={`dev-${id++}`}
				onClick={() => onClick(c)} >
				{Strings[c]}
				<div className="card-count">({count})</div>
			</span>
		);
	});
	return (
		<div className={"hand"}>
			{resource_cards}
			{dev_cards}
		</div>
	)
}