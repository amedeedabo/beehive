
export interface Hex {
	q: number,
	r: number,
	s: number,
}
export type TileIndex = Hex;
export enum VertexPlacement  {
	NORTHEAST = "NORTHEAST",
	SOUTHEAST = "SOUTHEAST"
}
export enum EdgeDirection {
	NORTHEAST,
	EAST,
	SOUTHEAST,
}
export interface VertexIndex extends TileIndex {
	orientation: VertexPlacement,
}
export interface EdgeIndex extends TileIndex {
	orientation: EdgeDirection,
}
/*
(u,v) → (u,v,N) (u,v,E) (u+1,v-1,W) (u,v-1,N) (u-1,v,E) (u,v,W)*/
export enum Direction  {
	WEST,
	NORTHWEST,
	SOUTHWEST,
	EAST,
	NORTHEAST,
	SOUTHEAST,
};
export enum DevCard  {
	Knight = "KNIGHT",
	YearOfPlenty = "PLENTY",
	Monopoly = "MONOPOLY",
	RoadBuilding = "ROAD_BUILDING",
	VictoryPoint = "VICTORY_POINT",
};
export enum Resource {
	Sheep = "Sheep",
	Wheat = "Wheat",
	Ore = "Ore",
	Tree = "Tree",
	Brick = "Brick",
	Desert = "Desert",
}
export interface Tile {
	position: TileIndex,
	resource: Resource,
	number: number,
}

export interface Edge { title: TileIndex, direction: Direction};
export interface Vertex { title: TileIndex, direction: Direction};

export interface Cell {
	position: TileIndex,
	resource: Resource,
	points: number,
}
export interface Port {
	center: TileIndex,
	edge: EdgeIndex,
	resource: Resource,
}