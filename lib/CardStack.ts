import { DevCard, Resource } from "./types"

export type CardStack<_T extends string> = { [index: string]: number }

export function newDevCardStack() {
	let res : CardStack<DevCard> = {}
	for (let dev in DevCard) {
		res[dev] = 0;
	}
	return res;
}
export function newResCardStack() {
	let res : CardStack<Resource> = {}
	for (let dev in Resource) {
		res[dev] = 0;
	}
	return res;
}
// export const emptyCardStack = <T extends string>() : CardStack<T>=> {
// 	return new Proxy({}, {
// 		get: (target:CardStack<T>, name: T) => name in target ? target[name] : 0
// 	})
// }
// /*
// export class CardStack<T extends string> {
	// In the future, we might use a Map, but for now
	// an object because BGIO requires our game state to be
	// serialiazable to JSON.
	// Agh this also applies to classes!
	// private stack: { [index: string]: number } = {}
	// constructor() {
		// this.stack = {}
	// }
	// */
//
export const StackHelpers = {
	set: <T extends string>(s: CardStack<T>, card: T, value: number) => {
		s[card] = value;
	},
	get: <T extends string>(s: CardStack<T>, card: T): number => {
		return s[card] ?? 0;
	},
	add: <T extends string>(s: CardStack<T>, card: T, num: number) => {
		s[card] += num;
	},
	inc: <T extends string>(s: CardStack<T>, card: T) => {
		StackHelpers.add(s, card, 1)
	},
	dec: <T extends string>(s: CardStack<T>, card: T) => {
		if (StackHelpers.get(s, card) < 1) {
			return;
		}
		s[card] -= 1;
	},
	to_array: <T extends string>(s: CardStack<T>): T[] => {
		let arr: T[] = [];
		for (let [card, count] of Object.entries(s)) {
			arr = arr.concat(Array(count).fill(card));
		}
		return arr;
	},
	entries: <T extends string>(s: CardStack<T>): [string, number][] => {
		return Object.entries(s)
	}
}