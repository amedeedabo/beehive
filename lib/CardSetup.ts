import { Resource, DevCard, TileIndex, Direction } from "./types";
import { shuffle } from "./util";

export type Distribution<T> = [T, number][];

export const StartingTiles : Distribution<Resource> = [
	[Resource.Sheep , 4],
	[Resource.Wheat , 4],
	[Resource.Tree , 4],
	[Resource.Ore , 3],
	[Resource.Brick , 3],
	[Resource.Desert , 1],
]

export const StartingNotches: Distribution<number> = [
	[2, 1],
	[3, 2],
	[4, 2],
	[5, 2],
	[6, 2],
	[8, 2],
	[9, 2],
	[10, 2],
	[11, 2],
	[12, 1],
]

export const StartingDevCards : Distribution<DevCard> = [
	[DevCard.Knight, 14],
	[DevCard.VictoryPoint, 5],
	[DevCard.YearOfPlenty, 2],
	[DevCard.Monopoly, 2],
	[DevCard.RoadBuilding, 2],
]
export const HarborPositions: [TileIndex, Direction][] = [
	[{ q:  0, r: 3, s: -3} , Direction.NORTHWEST],
	[{ q:  1, r:-3, s:  2} , Direction.SOUTHWEST], // 11:30
	[{ q: -1, r:-2, s:  3} , Direction.SOUTHEAST], // 11 o'clock
	[{ q: -3, r: 0, s:  3} , Direction.EAST], // 9
	[{ q: -3, r: 2, s:  1} , Direction.NORTHEAST], //7
	[{ q: -2, r: 3, s:  1} , Direction.NORTHEAST], // 6:30
	[{ q:  2, r: 1, s: -3} , Direction.WEST], // 3:30
	[{ q:  3, r:-1, s:  2} , Direction.WEST], // 2:30
	[{ q:  3, r:-3, s:  0} , Direction.SOUTHWEST], // 1
];
export const StartingHarbors : Distribution<Resource> = [
	[Resource.Sheep  , 1],
	[Resource.Wheat  , 1],
	[Resource.Tree   , 1],
	[Resource.Ore    , 1],
	[Resource.Brick  , 1],
	[Resource.Desert , 4],
]

export function buildByDistribution<T>(distribution : Distribution<T>) : T[] {
	let vals: T[] = distribution.flatMap(([res, num]) => Array(num).fill(res));
	shuffle(vals);
	return vals;
}