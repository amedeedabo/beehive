import { INVALID_MOVE } from 'boardgame.io/core';
import type { Ctx , PlayerID } from 'boardgame.io';
import equal from 'deep-equal';
import stringify from 'json-stable-stringify';
import { buildByDistribution, StartingDevCards, StartingNotches, StartingTiles, HarborPositions, StartingHarbors } from './CardSetup';
import { CardStack, newResCardStack, newDevCardStack } from './CardStack';
import { Cell, DevCard, EdgeIndex, Resource, TileIndex, VertexIndex, Port, Direction } from "./types";
import { addSet, generateHexagon, getCorners, getNeighbors, getOutboundEdges, getVertices, remove, getTileEdge } from "./util";
import { Context } from 'vm';
import { getLongestRoads } from './LongestRoad';

export interface GameState {
	cells: Cell[],
	dev_deck: DevCard[],
	roads: { [index: string]: PlayerID}, //RIP types, this should be a Map<TileIndex, PlayerID>
	settlements: PlayerOwned<VertexIndex>[],
	cities: PlayerOwned<VertexIndex>[],
	ports: Port[],
	robber: TileIndex,
	//have to use index: string here instead of index:PlayerID because index signatures can't be type aliases
	player_hands: { [index: string]: PlayerHand },
	buildable_vertices: VertexIndex[],
	rolls: number[],
	longest_road: PlayerID,
	largest_army: PlayerID,
}
export interface PlayerHand {
	resources: CardStack<Resource>,
	devcards: CardStack<DevCard>,
	knights_played: number,
	roads_built: number,
}
export interface PlayerOwned<T> {
	player: PlayerID,
	idx: T,
}

export function countPoints(G: GameState, idx: PlayerID, count_vp: boolean) : number {
	let points = 0;
	points += G.settlements.filter(s => s.player == idx).length;
	points +=  2 * G.cities.filter(c => c.player == idx).length;
	points += G.longest_road == idx ? 2: 0;
	points += G.largest_army == idx ? 2: 0;
	if (count_vp) {
		points += G.player_hands[idx].devcards[DevCard.VictoryPoint] ?? 0;
	}
	return points;
}

export function getBuildableRoads(G: GameState, playerID: PlayerID, withExtraRoad?: EdgeIndex) : EdgeIndex[] {
	let buildable_edges: Set<string> = new Set();
	function belongsTo<T>(val: PlayerOwned<T>) {
		return val.player == playerID
	}
	const player_buildings = G.settlements.filter(belongsTo).concat(G.cities.filter(belongsTo))
	for (let building of player_buildings){
		for (let edge of getOutboundEdges(building.idx)) {
			const edge_str = stringify(edge)
			if (!(edge_str in G.roads)) {
				buildable_edges.add(edge_str)
			}
		}
	}
	const roads_list : PlayerOwned<EdgeIndex>[] = Object.entries(G.roads).map(([s,id]): PlayerOwned<EdgeIndex> => ({player: id, idx: JSON.parse(s)}))
	const extraRoad : PlayerOwned<EdgeIndex> |undefined= withExtraRoad ? {player: playerID, idx: withExtraRoad} : undefined
	const player_roads = roads_list.filter(belongsTo)
	if(extraRoad) {
		player_roads.push(extraRoad);
	}
	//for each road the player owns
	//we get the two vertices it sits on
	//we ask for the neighboring edges
	//and we check if there is a road there
	for (let road of player_roads) {
		const road_ends = getVertices(road.idx);
		const neighboring_edges = road_ends.flatMap(road_end => getOutboundEdges(road_end))
		for (let edge of neighboring_edges) {
			console.log("Looking for un-built edge neighours of ", edge)
			//since we store strings, we can't ask if _any_ player has a road here, we have to go through the whole road_list
			if (!roads_list.some((r: PlayerOwned<EdgeIndex>) => equal(r.idx,edge))) {
				const edge_str = stringify(edge)
				//roads_list is constant, so it might let us add the same road twice, but buildable_edges
				//is a set so it won't take the duplicate.
				buildable_edges.add(edge_str)
			}
		}
	}
	if(extraRoad) {
		buildable_edges.delete(stringify(withExtraRoad))
	}
	return [...buildable_edges].map(s => JSON.parse(s))
}
export const createBoard = (): [Cell[], TileIndex, Port[]] => {
	const resources : Resource[] = buildByDistribution(StartingTiles);
	const notches : number[] = buildByDistribution(StartingNotches);
	const cells = generateHexagon(2);
	const tiles : Cell[] = [];
	let skipDesert = 0;
	for (let i = 0; i < resources.length; i++) {
		const resource = resources[i];
		let points = notches[i + skipDesert];
		if (resource == Resource.Desert) {
			skipDesert = -1;
			points = 0;
		}
		tiles.push({
			position: cells[i],
			resource,
			points,
		});
	}
	const robber: TileIndex = tiles.find(c => c.resource == Resource.Desert)?.position!;

	const port_resources = buildByDistribution(StartingHarbors);
	let ports: Port[] = HarborPositions.map(([center, direction]: [TileIndex, Direction], idx: number) => (
		{ center, edge: getTileEdge(center, direction), resource: port_resources[idx] }
	))
	return [tiles, robber, ports];
}


//vertex needs: is built or not, if built which buildingIdx, is buildable
const emptyHand = () : PlayerHand=> {
	return {
		resources: newResCardStack(),
		devcards: newDevCardStack(),
		knights_played: 0,
		roads_built: 0,
	}
}


function calculateLongestRoad(G: GameState, ctx: Context) {
	let longest_roads: number[] = [];
	for (let i = 0; i < ctx.numPlayers; i++) {
		const playerID: PlayerID = i.toString();
		const player_longest = getLongestRoads(G, playerID);
		console.log(`Player ${i} has longest road of length ${player_longest}`)
		G.player_hands[playerID].roads_built = player_longest;
		longest_roads.push(player_longest);
	}
	const max_longest_roads = Math.max(...longest_roads);
	if (max_longest_roads < 5) { //This may be necessary to reset longest road if someone loses it
		G.longest_road = ""
	}
	const contenders = Object.entries(G.player_hands).flatMap(([player, h]) => h.roads_built == max_longest_roads ? [player] : [])
	if (contenders.length == 1 && max_longest_roads >= 5) {
		G.longest_road = contenders[0];
	}
}

export const Game = {
	setup: (ctx: Ctx): GameState => {
		const [cells, robber, ports]  = createBoard();
		const buildable_vertices : VertexIndex[] = cells.flatMap(c => getCorners(c.position)).reduce(addSet, <VertexIndex[]>[])
		return {
			cells,
			ports,
			robber,
			buildable_vertices,
			dev_deck: buildByDistribution(StartingDevCards),
			settlements: [],
			cities: [],
			roads: {},
			player_hands: Object.fromEntries(Array(ctx.numPlayers).fill(0).map(emptyHand).map((hand, idx) => [idx, hand])),
			rolls: [],
			longest_road: '',
			largest_army: '',
		}
	},

	moves: {
		/*
		Todo:
		* Propose+accept trade
		* Two part Year of Plenty

		*/

		buildSettlement: (G: GameState, ctx: Ctx, pos: VertexIndex) => {
			// const cost = [Resource.Tree, Resource.Brick];
			if (!G.buildable_vertices.some(v => equal(v, pos))) {
				console.log(`Asked to build on position ${pos} but it is not in buildable vertices.`)
				return INVALID_MOVE;
			}
			remove(G.buildable_vertices, pos);
			G.settlements.push({player: ctx.currentPlayer, idx: pos});
			for (let neigh of getNeighbors(pos)) {
				remove(G.buildable_vertices, neigh)
			}
			calculateLongestRoad(G, ctx);
		},
		buildCity: (G: GameState, ctx: Ctx, pos: VertexIndex) => {
			// const cost = [Resource.Wheat, Resource.Wheat, Resource.Ore, Resource.Ore, Resource.Ore];
			const to_upgrade = {player: ctx.currentPlayer, idx: pos}
			if (!G.settlements.some(v => equal(v, to_upgrade))) {
				console.log(`Player ${ctx.currentPlayer} asked to upgrade settlement at ${pos} but they do not have a settlement there.`)
				return INVALID_MOVE;
			}
			remove(G.settlements, to_upgrade);
			G.cities.push(to_upgrade);
		},
		buildRoad: (G: GameState, ctx: Ctx, pos: EdgeIndex) => {
			// const cost = [Resource.Brick, Resource.Tree];
			const player_buildable = getBuildableRoads(G, ctx.currentPlayer)
			if (!player_buildable.some(v => equal(v, pos))) {
				console.log(`Player ${ctx.currentPlayer} asked to build road at ${pos} but that is not in their buildable road set.`, )
				return INVALID_MOVE;
			}
			G.roads[stringify(pos)] = ctx.currentPlayer;
			calculateLongestRoad(G, ctx);
		},
		buyDevCard: (G: GameState, ctx: Ctx) => {
			// const cost = [Resource.Sheep, Resource.Wheat, Resource.Ore];
			console.log(`Buy Devcard called for player ${ctx.currentPlayer}`)
			if (G.dev_deck.length < 1) {
				return INVALID_MOVE
			}
			const card: DevCard= G.dev_deck.pop()!;
			G.player_hands[ctx.currentPlayer].devcards[card] = (G.player_hands[ctx.currentPlayer].devcards[card] ?? 0) + 1;
			console.log(`dev card ${card} bought, player hand now has ${G.player_hands[ctx.currentPlayer].devcards[card]} of this card`)
		},
		rollDice: (G: GameState, _ctx: Ctx) => {
			//TODO: only one roll per turn
			const roll = (Math.floor(Math.random() * 6) + 1) +
						 (Math.floor(Math.random() * 6) + 1)
			const matching_tiles = G.cells.filter(cell => cell.points == roll)
			for (let tile of matching_tiles) {
				if (equal(G.robber, tile.position)) {
					console.log("Skipping tile because the robber is on it: ", tile.position)
					continue
				}
				const corners = getCorners(tile.position)
				const matching_settlements = G.settlements.filter(b => corners.filter(corner => equal(corner, b.idx)).length != 0)
				for(let settlement of matching_settlements) {
					G.player_hands[settlement.player].resources[tile.resource] += 1;
				}
				const matching_cities = G.cities.filter(b => corners.filter(corner => equal(corner, b.idx)).length != 0)
				for(let city of matching_cities) {
					G.player_hands[city.player].resources[tile.resource] += 2;
				}
			}
			G.rolls.push(roll)
		},
		useDevCard: (G: GameState, ctx: Ctx, card: DevCard, card_params: any) => {
			console.log(`Use Devcard ${card} called for player ${ctx.currentPlayer}`)
			const currentHand = G.player_hands[ctx.currentPlayer]
			if(currentHand.devcards[card] < 1) {
				return INVALID_MOVE
			}
			switch(card) {
				case DevCard.VictoryPoint :
					return INVALID_MOVE;
					break;
				case DevCard.Knight:
					// assert(card_params.position instanceof Hex)
					const { position } = card_params
					G.robber = position;
					//const hand_to_steal = G.player_hands[player].resources
					//const to_steal_arr = hand_to_steal.to_array()
					//const stolen_idx = Math.floor(Math.random() * to_steal_arr.length)
					//const stolen_card = to_steal_arr[stolen_idx]
					//hand_to_steal.dec(stolen_card)
					//currentHand.resources.inc(stolen_card)
					currentHand.knights_played += 1;
					//We can apply the largest army rules (most knights card played and first to the current number if there is a tie)
					//By only switching largest army if there is not a tie, and if the number is > 3
					const most_knights_played = Math.max(...Object.values(G.player_hands).map(h => h.knights_played))
					const largest_contenders = Object.entries(G.player_hands).flatMap(([player, h]) => h.knights_played == most_knights_played? [player] : [])
					if (largest_contenders.length == 1 && most_knights_played >=3 ) {
						G.largest_army = largest_contenders[0].toString();
					}
					break;
				case DevCard.RoadBuilding:
					const {road_1, road_2} = card_params
					G.roads[stringify(road_1)] = ctx.currentPlayer;
					G.roads[stringify(road_2)] = ctx.currentPlayer;
					break;
				case DevCard.YearOfPlenty:
					//TODO: check that card params are valid resources
					const {res_1, res_2} = card_params
					currentHand.resources[res_1] += 1;
					currentHand.resources[res_2] += 2;
					break;
				case DevCard.Monopoly:
					//TODO
					const {resource} = card_params
					let num_stolen = 0;
					for(let idx in G.player_hands) {
						if (idx == ctx.currentPlayer) {
							continue
						}
						let hand = G.player_hands[idx]
						num_stolen += hand.resources[resource];
						hand.resources[resource] = 0;
					}
					currentHand.resources[resource] +=  num_stolen;
			}
			currentHand.devcards[card] -= 1;
			console.log(`dev card ${card} used, player hand now has ${currentHand.devcards[card]} of this card`)
		}
	},
	endIf: (G: GameState, ctx: Ctx) => {
		if (countPoints(G, ctx.currentPlayer, true) >= 10) {
			return { winner: ctx.currentPlayer }
		}
	},

	ai: {
		enumerate: (G: GameState, _ctx: Ctx) => {
			return G.cells.flatMap((c, idx) => (c !== null) ? [] : {move: 'clickCell', args: [idx]});
		}
	},
};