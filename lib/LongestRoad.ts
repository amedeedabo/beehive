import { GameState } from "./Game";
import { PlayerID } from "boardgame.io";
import { IRoad, ITown, getLongestRoadLength, Color, IEdge} from "./vendorLongestRoad"
import { getCorners, getVertices } from "./util";
import stringify from "json-stable-stringify";
import { EdgeIndex } from "./types";

/*
function deepClone<T>(obj: T): T {
    return JSON.parse(JSON.stringify(obj))
}
*/

/*Not writing my own code yet.
Going to translate my types into the vendorLongestRoad types, call
getLongestRoadLength(playerRoads: IRoad[], allTowns: ITown[]){
n there, and return the result back.
*/

// const playerColors = { "0": "red", "1": "blue", "2": "green", "3": "yellow" }
const playerColors: { [index: string]: Color } = { "0": "red", "1": "blue", "2": "green", "3": "orange" }
// const playerColors = ["red", "blue", "green", "yellow", "black", "purple"]
function toIEdge(allVertices: string[], edge: EdgeIndex): IEdge {
	const [p1, p2] = getVertices(edge);
	return [allVertices.findIndex(v => v == stringify(p1)), allVertices.findIndex(v => v == stringify(p2))]
}
export function getLongestRoads(G: GameState, playerID: PlayerID) : number {
	const allVertices = [... new Set(G.cells.flatMap(c => getCorners(c.position)).map(v => stringify(v)))];

    const playerRoads: IRoad[] = Object.entries(G.roads)
        .flatMap(([edge_str, player]) => player == playerID ? [{
            edge: toIEdge(allVertices, JSON.parse(edge_str)),
            color: playerColors[player]
		}] : [] );

    const iTownSettlements: ITown[] = G.settlements.map((s): ITown => ({
        vertex: allVertices.findIndex(v => v == stringify(s.idx)),
        color: playerColors[s.player],
        isCity: false,
        isPort: false
    }));
    const iTownCities: ITown[] = G.cities.map((s): ITown => ({
        vertex: allVertices.findIndex(v => v == stringify(s.idx)),
        color: playerColors[s.player],
        isCity: true,
        isPort: false
    }));
    const allTowns = iTownCities.concat(iTownSettlements);
    // getLongestRoadLength(playerRoads: IRoad[], allTowns: ITown[])
    return getLongestRoadLength(playerRoads, allTowns);
}
/*A Road set
function getConnectedComponentLength(roads: EdgeIndex[]) {
    let to_visit : EdgeIndex[] = [deepClone(roads[0])]
    //each road
    while(to_visit.length > 0) {
        current_road

    }
}
*/
/*
function getPlayerLongestRoad(G: GameState, playerID: PlayerID) : number {
	const player_roads : EdgeIndex[] = Object.entries(G.roads).filter(([_s, id]) => id == playerID).map(([s,_id]) => JSON.parse(s));

	//split the roads into different connected components
	let road_components = new Array<Set<EdgeIndex>>();
	let current_component = new Set<EdgeIndex>();
	const is_cutoff = (v: VertexIndex) => G.settlements.some(s => s.idx == v && s.player != playerID) || G.cities.some(s => s.idx == v && s.player != playerID)

	let to_visit : EdgeIndex[] = deepClone(player_roads);
	for (let road of to_visit) {
		const [v1, v2] = getVertices(road);
		let v1_cutoff = is_cutoff(v1)
		let v2_cutoff = is_cutoff(v2)
		let v1_connected = current_component.has(v1);
		let v2_connected = current_component.has(v2);
		//we use the current component if we have at least one end that is connected and not cutoff
		const use_current = (v1_connected && !v1_cutoff) || (v2_connected && !v2_cutoff)
		if (use_current) {
			current_component.add(v1)
			current_component.add(v2)
		}
		else {


		}
		let found = false;
		for (let comp of road_components) {
			if (comp.has(v1) || comp.has(v2)) {
				comp.add(v1);
				comp.add(v2);
				found = true;
				break;
			}
		}
		if (!found) {
			let gt =
			road_components

		}
	}
	return 0
}
*/