import {DevCard, Resource} from "./types"
export const CatanStrings = {
    [Resource.Brick] : "Brick",
    [Resource.Tree]: "Wood",
    [Resource.Sheep] : "Sheep",
    [Resource.Wheat] : "Wheat",
    [Resource.Ore] : "Ore",
    [Resource.Desert] : "Desert",
    [DevCard.VictoryPoint] : "Victory Point",
    [DevCard.Knight] : "Knight",
    [DevCard.Monopoly] : "Monopoly",
    [DevCard.RoadBuilding] : "Road Building",
    [DevCard.YearOfPlenty] : "Year of Plenty",
}
export const CatanFiles = {
	[Resource.Sheep]: "sheep.jpeg",
	[Resource.Wheat]: "wheat.jpeg",
	[Resource.Ore]: "rock.jpeg",
	[Resource.Tree]: "tree.jpeg",
	[Resource.Brick]: "brick.jpeg",
	[Resource.Desert]: "desert.jpeg",
}
export const BeeStrings = {
    [Resource.Brick] : "Beeswax",
    [Resource.Tree]: "Worker Bee",
    [Resource.Sheep] : "Pollen",
    [Resource.Wheat] : "Nectar",
    [Resource.Ore] : "Royal Jelly",
    [Resource.Desert] : "Desert",
    [DevCard.VictoryPoint] : "Victory Point",
    [DevCard.Knight] : "Queen Bee",
    [DevCard.Monopoly] : "Monopoly",
    [DevCard.RoadBuilding] : "Hive Building",
    [DevCard.YearOfPlenty] : "Year of Plenty",
}