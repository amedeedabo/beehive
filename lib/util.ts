import {Hex, TileIndex, VertexPlacement, VertexIndex, EdgeIndex, EdgeDirection, Direction} from './types'
import equal from 'deep-equal'

export function shuffle<T>(a: T[]) : T[] {
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
}

export function addSet<T>(arr: T[], value: T): T[] {
  const index = arr.findIndex(arr_val => equal(arr_val, value));
  if (index == -1) {
    arr.push(value)
  }
  return arr;
}
export function remove<T>(arr: T[], value: T): T[] {
  const index = arr.findIndex(arr_val => equal(arr_val, value));
  if (index > -1) {
    arr.splice(index, 1);
  }
  return arr;
}
//taken from react-hexgrid
export function generateHexagon(mapRadius: number) : Hex[] {
    let hexas: Hex[] = [];
    for (let q = -mapRadius; q <= mapRadius; q++) {
      let r1 = Math.max(-mapRadius, -q - mapRadius);
      let r2 = Math.min(mapRadius, -q + mapRadius);
      for (let r = r1; r <= r2; r++) {
        hexas.push({q, r, s: -q-r});
      }
    }

    return hexas;
  }

export function getCorners({ q, r, s }: TileIndex): VertexIndex[] {
/* The points are taken in this order, from these tiles
      / \
     /   \
    |     |
    |q,r-1|
    |     6
   / \   / \
  /   \ /   \
 |     3     1
 |q-1,r| q,r |
 |     4     2
  \   / \   /
   \ /   \ /
    | q-1 5
    | r+1 |
    |     |
     \   /
      \ /
       V
*/
	return  [
		{q: q    , r: r    , s: s    , orientation: VertexPlacement.NORTHEAST},
		{q: q    , r: r    , s: s    , orientation: VertexPlacement.SOUTHEAST},
		{q: q - 1, r: r    , s: s + 1, orientation: VertexPlacement.NORTHEAST},
		{q: q - 1, r: r    , s: s + 1, orientation: VertexPlacement.SOUTHEAST},
		{q: q    , r: r - 1, s: s + 1, orientation: VertexPlacement.SOUTHEAST},
		{q: q - 1, r: r + 1, s: s    , orientation: VertexPlacement.NORTHEAST},

	]
}
export function getTiles({ q, r, s , orientation}: VertexIndex): TileIndex[] {
  /*
     / \
    /   \
   | q+1 |
   | r-1 |
   .  C  .
  / \   / \
 /   \ /   \
|  A  .  B  1
| q,r |q+1,r|
|     .     2
 \   / \   /
  \ / D \ /
   . q   .
   | r+1 |
   |     |
    \   /
     \ /
      V
*/

let neighs = [
			{q: q    , r: r    , s: s    }, //A
			{q: q + 1, r: r    , s: s - 1}, //B
]
	if (orientation == VertexPlacement.NORTHEAST) {
    neighs.push(
			{q: q + 1, r: r - 1, s: s    }, //C
    )
	} else {
    neighs.push(
			{q: q    , r: r + 1, s: s - 1}, //D
    )
  }
  return neighs
}

export function getNeighbors({ q, r, s, orientation }: VertexIndex): VertexIndex[] {
/*The points are taken in this order, from these tiles
     / \
    /   \
   |     |
   |q,r-1|
   A     B
  / \   / \
 /   \ /   \
|     O     1
|q-1,r| q,r |
|     C     2
 \   / \   /
  \ /   \ /
   E q-1 F
   | r+1 |
   |     |
    \   /
     \ /
      V
*/
	if (orientation == VertexPlacement.NORTHEAST) {
		return [
			({q: q    , r: r - 1, s: s + 1, orientation: VertexPlacement.SOUTHEAST }),
			({q: q + 1, r: r - 1, s: s    , orientation: VertexPlacement.SOUTHEAST }),
			({q: q    , r: r    , s: s    , orientation: VertexPlacement.SOUTHEAST }),
		]
	} else {
		return [
			{q: q    , r: r    , s: s    , orientation: VertexPlacement.NORTHEAST },
			{q: q - 1, r: r + 1, s: s    , orientation: VertexPlacement.NORTHEAST },
			{q: q    , r: r + 1, s: s - 1, orientation: VertexPlacement.NORTHEAST },
		]
	}
}
export function getEdges({ q, r, s }: TileIndex): EdgeIndex[] {
/*The points are taken in this order, from these tiles
     / \
    /   \
   |     |
   |q,r-1|
   |     |
  / \   / \
 /   \SE  NE
|     |     |
|q-1,rE q,r E
|     |     |
 \   /NE  SE
  \ /   \ /
   | q-1 |
   | r+1 |
   |     |
    \   /
     \ /
      V
*/
  return [
			{ q: q    , r: r    , s: s    , orientation: EdgeDirection.EAST },
			{ q: q    , r: r    , s: s    , orientation: EdgeDirection.NORTHEAST },
			{ q: q    , r: r    , s: s    , orientation: EdgeDirection.SOUTHEAST },
			{ q: q - 1, r: r    , s: s + 1, orientation: EdgeDirection.EAST },
			{ q: q - 1, r: r + 1, s: s    , orientation: EdgeDirection.NORTHEAST },
			{ q: q    , r: r - 1, s: s + 1, orientation: EdgeDirection.SOUTHEAST },
  ]
}
export function getVertices({ q, r, s , orientation}: EdgeIndex): [VertexIndex, VertexIndex] {
/*The points are taken in this order, from these tiles
     / \
    /   \
   |     |
   |q,r-1|
   |     3
  / \   / \
 /   \SE  NE
|     |     1
|q-1,rE q,r E
|     |     2
 \   /NE  SE
  \ /   \ /
   | q-1 4
   | r+1 |
   |     |
    \   /
     \ /
      V
*/
  switch(orientation) {
    case EdgeDirection.EAST:
      return [
          ({q: q    , r: r    , s: s    , orientation: VertexPlacement.NORTHEAST }), // 1
          ({q: q    , r: r    , s: s    , orientation: VertexPlacement.SOUTHEAST }), // 2
      ]
    break;
    case EdgeDirection.NORTHEAST:
      return [
          ({q: q    , r: r    , s: s    , orientation: VertexPlacement.NORTHEAST }), // 1
          ({q: q    , r: r - 1, s: s + 1, orientation: VertexPlacement.SOUTHEAST }), // 3
      ]
    break;
    case EdgeDirection.SOUTHEAST:
      return [
          ({q: q    , r: r    , s: s    , orientation: VertexPlacement.SOUTHEAST }), // 2
          ({q: q - 1, r: r + 1, s: s    , orientation: VertexPlacement.NORTHEAST }), // 2
      ]
    break;
  }
}
export function getOutboundEdges({ q, r, s, orientation}: VertexIndex): EdgeIndex[] {
/*The points are taken in this order, from these tiles
     / \
    /   \
   | q+1 |
   | r-1 |
   |     3
  / \   / \
 /   B C  NE
|     | q+1 |
| q,r A  r  |
|     |     |
 \   D E   /
  \ /   \ /
   |  q  4
   | r+1 |
   |     |
    \   /
     \ /
      V
*/
let outbound = [
  {q, r, s, orientation: EdgeDirection.EAST} //A
]
if (orientation == VertexPlacement.NORTHEAST) {
  outbound.push({ q, r, s, orientation: EdgeDirection.NORTHEAST }) //B
  outbound.push({ q: q + 1, r: r - 1, s, orientation: EdgeDirection.SOUTHEAST }) //C
} else {
  outbound.push({q, r, s,orientation: EdgeDirection.SOUTHEAST}) //D
  outbound.push({ q, r: r + 1, s: s -1, orientation: EdgeDirection.NORTHEAST }) //E
}
return outbound
}
export function getTileEdge(tile: TileIndex, direction: Direction): EdgeIndex {
  const { q, r, s} = tile;
  switch(direction) {
    //these are all contained in EdgeDirection
    case Direction.NORTHEAST:
      return {...tile, orientation: EdgeDirection.NORTHEAST}
    case Direction.EAST:
      return {...tile, orientation: EdgeDirection.EAST}
    case Direction.SOUTHEAST:
      return {...tile, orientation: EdgeDirection.SOUTHEAST}
    case Direction.SOUTHWEST:
      return { q: q - 1, r: r + 1, s: s    , orientation: EdgeDirection.NORTHEAST }
    case Direction.WEST:
      return { q: q - 1, r: r    , s: s + 1, orientation: EdgeDirection.EAST }
    case Direction.NORTHWEST:
      return { q: q    , r: r - 1, s: s + 1, orientation: EdgeDirection.SOUTHEAST }
  }
}