import {createBoard, Game} from '../Game';
import { Resource } from '../types';
// function buildByDistribution<T>(distribution : {[key: string] : number}) : T[] {
// 	let vals: T[] = Object.keys(distribution).flatMap(res => Array(distribution[res]).fill(res));
// 	shuffle(vals);
// 	return vals;
// }
// test('this is jest' , () => {
// 	expect(1).toBe(1);
// });

test('The desert never has any points' , () => {
    for(let i = 0; i < 100; i++) {
        const cells = createBoard();
        cells.map(c => {
            if (c.resource == Resource.Desert) {
                expect(c.points).toBe(0);
            } else {
                expect(c.points).not.toBeUndefined();
                // expect(c.points).toBeGreaterThan(0);
            }
        })
    }
});
test('The robber starts on the desert' , () => {
    for(let i = 0; i < 100; i++) {
        // const G = Game.setup({}))
        // expect(G.cells.find(c => c.resource == Resource.Desert)?.position).toBe(G.robber)
    }
});