# Beehive - a Catan Clone in JS

![Screenshot of the board](screenshot.png)

Uses boardgame.io to handle board game logic.

Todo

- [x] All buildable entities (Road, Farm, City)
- [x] Buy Dev card
- [x] Roll Dice
- [ ] Discard on Roll 7 (requires boardgameio stages)
- [x] Roll on robber tile blocks that tile from producing
- [ ] Moving robber steals card from a player on that tile
- [x] All development cards implemented
- [x] Show player their own private score
- [ ] Count Longest Road
- [ ] Supply ("bank") totals
- [ ] Trading
- * [ ] Propose a trade
- * [ ] Accept a trade
- * [ ] Trade with the bank
- [ ] Ports
- * [ ] Display Ports
- * [ ] Owning ports changes trade ratios

* Finished Catan logic
* Use boardgame.io's MCTS to build an AI
* Write the logic in Rust/wasm to speed up the AI, if needed
* Handle mutliplayer with backend server
* Handle multiplayer for people on your wifi (behind the same NAT) through static hosting
* Handle multiplayer anywhere through static hosting and a CoTurn server, webRTC

Running
------
```bash
npm install
npm run dev
# or
yarn
yarn dev
```

Known missing rule bugs
------
* If a player has the Build 2 Roads dev card, and they do not have the space to build both roads, we don't handle that. They should be able
to only build 1 road.
* If the supply has run out or does not have enough of a particular resource to supply everyone, we should not supply.