import React from 'react';
import { Client } from 'boardgame.io/react';
import { SocketIO } from 'boardgame.io/multiplayer'
import {Game} from '../lib/Game';
import {Board} from '../components/Board';
import Head from 'next/head'

const TTTClient = Client({
	game: Game,
	board: Board,
	multiplayer: SocketIO({ server: 'localhost:8000' }),
});

export default function Home () {
	return (
			<>
			<Head>
				<title>Beehive</title>
			</Head>
			<TTTClient playerID="0" />
			<TTTClient playerID="1" />
			</>
	       )
}